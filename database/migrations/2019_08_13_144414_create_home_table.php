<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing home
        Schema::create('home', function (Blueprint $table) {
                            $table->increments('id');
                                    
                    });
                    Schema::create('home_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('home_id')->unsigned();
                            $table->string('banner_title')->unique();
                            $table->text('banner_description')->nullable();
                            $table->text('banner_image');
                            $table->string('publishers_title')->nullable();
                            $table->string('box_title_1')->nullable();
                            $table->string('box_title_2')->nullable();
                            $table->string('box_title_3')->nullable();
                            $table->string('clients_title')->nullable();
                            $table->string('box_title_4')->nullable();
                            $table->string('box_title_5')->nullable();
                            $table->string('box_title_6')->nullable();
                            $table->string('address')->nullable();
                            $table->string('tel')->nullable();
                            $table->string('email')->nullable();
                        $table->string('locale')->index();


            $table->unique(['home_id','locale']);
            $table->foreign('home_id')->references('id')->on('home')->onDelete('cascade');

            });
            }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
                Schema::dropIfExists('home_translations');
                Schema::dropIfExists('home');
    }
}
