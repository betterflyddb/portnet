@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                <li class="breadcrumb-item active">Home</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Home</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("home.update",[$data->id]) }}" method="post"
                                          enctype="multipart/form-data">
                                                                                    @method('PUT')
                                                                                @csrf
                                        @php
    if(isset($data)){
        $value = $data->banner_title;
        $value = key_exists('banner_title',old()) ? old('banner_title') : $value;
    } else{
        $value = old('banner_title');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Banner Title</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="string" name="banner_title" placeholder="Banner Title">
        @if($errors->get('banner_title'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('banner_title') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->banner_description;
        $value = key_exists('banner_description',old()) ? old('banner_description') : $value;
    } else{
        $value = old('banner_description');
    }
@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="text-input">Banner Description</label>
    <div class="col-md-9">
        <textarea rows="5" cols="50" class="form-control" name="banner_description" placeholder="Banner Description">{{ $value }}</textarea>
        @if($errors->get('banner_description'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('banner_description') }}</div>
        @endif
    </div>
</div>@php
    if(isset($data)){
        $files =  json_decode($data->banner_image) ? json_decode($data->banner_image) : ($data->banner_image && $data->banner_image !== 'none' ?  [$data->banner_image]: []);
        $value =  $data->banner_image;
    }else{
        $value = null;
        $files = [];
    }

@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="fileField_2">Banner Image</label>
    <div class="col-md-9">
        <input for="banner_image" name="fileField_2" id="fileField_2" type="file">
        <input type="hidden" name="banner_image" {{ $value ? "value=".$value : "value=none" }}>
        <br>
        <br>
        @foreach($files as $key => $file)
            <div class="preview-container">
                <img class="old file-preview {{ @strpos(mime_content_type('storage/uploads/files/'.$file),'image') !== 0 ? 'filetype-file' : 'filetype-image' }}" data-src="{{ 'storage/uploads/files/'.$file }}" src="{{ 'storage/uploads/files/'.$file }}" height="150">
                <a data-index="{{ $key }}" href="javascript:;" class="remove-image">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        @endforeach
    </div>
</div>
@push('scripts')
<script>
  loadCss(['../vendor/betterfly/plugins/filePlugin/filePlugin.css']);
  loadScript(['../vendor/betterfly/plugins/filePlugin/filePlugin.js'], loadfileField_2);

  function loadfileField_2() {
    $('input#fileField_2').filePlugin({
      maxCount: 1,
      mimeTypes: ["jpg","jpeg","png","svg",],
      folder: "files",
      thumbs: [],
      required: true
    });
  }
</script>
@endpush@php
    if(isset($data)){
        $value = $data->publishers_title;
        $value = key_exists('publishers_title',old()) ? old('publishers_title') : $value;
    } else{
        $value = old('publishers_title');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">For Publishers Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="publishers_title" placeholder="For Publishers Title">
        @if($errors->get('publishers_title'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('publishers_title') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->box_title_1;
        $value = key_exists('box_title_1',old()) ? old('box_title_1') : $value;
    } else{
        $value = old('box_title_1');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">First Circle Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="box_title_1" placeholder="First Circle Title">
        @if($errors->get('box_title_1'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('box_title_1') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->box_title_2;
        $value = key_exists('box_title_2',old()) ? old('box_title_2') : $value;
    } else{
        $value = old('box_title_2');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Second Circle Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="box_title_2" placeholder="Second Circle Title">
        @if($errors->get('box_title_2'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('box_title_2') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->box_title_3;
        $value = key_exists('box_title_3',old()) ? old('box_title_3') : $value;
    } else{
        $value = old('box_title_3');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Third Circle Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="box_title_3" placeholder="Third Circle Title">
        @if($errors->get('box_title_3'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('box_title_3') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->clients_title;
        $value = key_exists('clients_title',old()) ? old('clients_title') : $value;
    } else{
        $value = old('clients_title');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">For Clinets Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="clients_title" placeholder="For Clinets Title">
        @if($errors->get('clients_title'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('clients_title') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->box_title_4;
        $value = key_exists('box_title_4',old()) ? old('box_title_4') : $value;
    } else{
        $value = old('box_title_4');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">First Circle Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="box_title_4" placeholder="First Circle Title">
        @if($errors->get('box_title_4'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('box_title_4') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->box_title_5;
        $value = key_exists('box_title_5',old()) ? old('box_title_5') : $value;
    } else{
        $value = old('box_title_5');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Second Circle Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="box_title_5" placeholder="Second Circle Title">
        @if($errors->get('box_title_5'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('box_title_5') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->box_title_6;
        $value = key_exists('box_title_6',old()) ? old('box_title_6') : $value;
    } else{
        $value = old('box_title_6');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Third Circle Title</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="box_title_6" placeholder="Third Circle Title">
        @if($errors->get('box_title_6'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('box_title_6') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->address;
        $value = key_exists('address',old()) ? old('address') : $value;
    } else{
        $value = old('address');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Address</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="address" placeholder="Address">
        @if($errors->get('address'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('address') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->tel;
        $value = key_exists('tel',old()) ? old('tel') : $value;
    } else{
        $value = old('tel');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Tel</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="tel" placeholder="Tel">
        @if($errors->get('tel'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('tel') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->email;
        $value = key_exists('email',old()) ? old('email') : $value;
    } else{
        $value = old('email');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Email</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="email" placeholder="Email">
        @if($errors->get('email'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
        @endif
    </div>
</div>

                                        <input type="hidden" value="\App\Modules\Home\HomeRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection