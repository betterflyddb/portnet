<!DOCTYPE html>
<html>
<head>
    <title>PortNet</title>
    <link rel="stylesheet" href="{{ asset('css/fullpage.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/reboot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/ico" href="{{ asset('images/favicon.png') }}" />

    <meta name="description" content="@lang('site_description')"/>
    <meta name="keywords"  content="@lang('site_keyword')"/>
    <meta name="resource-type" content="document"/>

    <meta property="og:title" content="@lang('site_title')" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:image" content="{{ asset('images/og-image.png') }}" />
    <meta property="og:site_name" content="portnet.tech" />

</head>
<body>
<header>
    <div class="logo">
        <a href="">
            <img src="{{ asset('images/logo.png') }}" alt="logo">
        </a>
    </div>
    <nav id="menu">
        <li data-menuanchor="publisher"><a href="#publisher">@lang('Publisher')</a></li>
        <li data-menuanchor="advertiser"><a href="#advertiser">@lang('Advertiser')</a></li>
        <li data-menuanchor="contact"><a href="#contact">@lang('Contact us')</a></li>
    </nav>
</header>
<div id="fullpage">
    <div class="section active" id="section0"
         style="background-image: url({{ asset('storage/uploads/files/'.$data->banner_image) }})">
        <div class="intro">
            <h2>
                {!! $data->banner_title !!}
            </h2>
            <span>{!! $data->banner_description !!}</span>
        </div>
        <div id="particles-js"></div>
    </div>
    <div class="section" id="section1">
        <div class="for-publishers">
            <h2>@lang('for publishers')</h2>
        </div>
        <div class="information-publishers">
            <div class="title">{!! $data->publishers_title !!}</div>
            <div class="circle-container">
                <div class="circle-block">
                    <span>{!! $data->box_title_1 !!}</span>
                    <div class="dot dot1"></div>
                    <div class="dot dot2"></div>
                    <div class="dot dot3"></div>
                    <div class="dot dot4"></div>
                    <div class="dot dot5"></div>
                    <div class="dot dot6"></div>
                </div>
                <div class="circle-block">
                    <span>{!! $data->box_title_2 !!}</span>
                    <div class="dot dot1"></div>
                    <div class="dot dot2"></div>
                    <div class="dot dot3"></div>
                    <div class="dot dot4"></div>
                    <div class="dot dot5"></div>
                    <div class="dot dot6"></div>
                </div>
                <div class="circle-block">
                    <span>{!! $data->box_title_3 !!}</span>
                    <div class="dot dot1"></div>
                    <div class="dot dot2"></div>
                    <div class="dot dot3"></div>
                    <div class="dot dot4"></div>
                    <div class="dot dot5"></div>
                    <div class="dot dot6"></div>
                </div>
            </div>
            <div class="bottom-container">
                <div class="bottom-block">
                    @lang('Contact Us')
                </div>
                <div class="bottom-block">
                    @lang('Implement Tag')
                </div>
                <div class="bottom-block">
                    @lang('Star Making Money')
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section2">
        <div class="for-clients">
            <h2>@lang('for Clients')</h2>
        </div>
        <div class="information-clients">
            <div class="title">{!! $data->clients_title !!}</div>
            <div class="circle-container">
                <div class="circle-block">
                    <span>{!! $data->box_title_4 !!}</span>
                    <div class="dot dot1"></div>
                    <div class="dot dot2"></div>
                    <div class="dot dot3"></div>
                    <div class="dot dot4"></div>
                    <div class="dot dot5"></div>
                    <div class="dot dot6"></div>
                </div>
                <div class="circle-block">
                    <span>{!! $data->box_title_5 !!}</span>
                    <div class="dot dot1"></div>
                    <div class="dot dot2"></div>
                    <div class="dot dot3"></div>
                    <div class="dot dot4"></div>
                    <div class="dot dot5"></div>
                    <div class="dot dot6"></div>
                </div>
                <div class="circle-block">
                    <span>{!! $data->box_title_6 !!}</span>
                    <div class="dot dot1"></div>
                    <div class="dot dot2"></div>
                    <div class="dot dot3"></div>
                    <div class="dot dot4"></div>
                    <div class="dot dot5"></div>
                    <div class="dot dot6"></div>
                </div>
            </div>
            <div class="bottom-container">
                <div class="bottom-block">
                    @lang('Contact Us')
                </div>
                <div class="bottom-block">
                    @lang('Choose Site Categories')
                </div>
                <div class="bottom-block">
                    @lang('Grown Your Buisness')
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section3">
        <div class="contact-container">
            <div class="left">
                <div class="get-in-touch">
                    get<br>in<br>touch
                </div>
                <div class="highlite">
                    <span>@lang('Contact us, choose your prefered ad unit, and turn your website into a revenue stream.')</span>
                </div>
                <div class="address">
                    <img src="{{ asset('images/map.png') }}">
                    <span>{{ $data->address }} {{ $data->tel }}; {{ $data->email }}.</span>
                    <a target="_blank" href="@lang('direction_link')">@lang('Get directions')</a>
                </div>
            </div>
            <div class="right">
                <form action="javascript:;" method="post">
                    <div class="radio-buttons">
                        <div class="publisher">
                            <input type="radio" id="publisher-checkbox" name="type" value="@lang('Publisher')" checked>
                            <label for="publisher-checkbox">@lang('Publisher')</label>
                        </div>
                        <div class="advertiser-checkbox">
                            <input type="radio" id="advertiser-checkbox" value="@lang('Advertiser')" name="type">
                            <label for="advertiser-checkbox">@lang('Advertiser')</label>
                        </div>
                    </div>
                    <div class="input-fields">
                        <input type="text" name="name" placeholder="Name *" required autocomplete="Off">
                        <input type="email" name="email" placeholder="Email *" required autocomplete="Off">
                        <textarea class="message" placeholder="Message *" required></textarea>
                    </div>
                    <input type="submit" style="margin-bottom:15px" class="send-button" value="@lang('Send')">
                    <div class="alert-message" style="margin-left:6px; min-height: 25px;"></div>
                </form>
            </div>
        </div>
        <div class="contact-bottom">
            <div class="left">
                <a href="@lang('fb_link')" target="_blank">
                    <img src="{{ asset('images/fb.png') }}" alt="fb">
                </a>
                <a href="@lang('instagram_link')" target="_blank">
                    <img src="{{ asset('images/instagram.png') }}" alt="instagram">
                </a>
                <a href="@lang('youtube_link')" target="_blank">
                    <img src="{{ asset('images/youtube.png') }}" alt="youtube">
                </a>
            </div>
            <div class="right">
                &copy; {{ date('Y') }} PortNet
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/fullpage.min.js') }}"></script>
<script src="{{ asset('js/particles.min.js') }}"></script>
<script src="{{ asset('js/particles-config.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#fullpage').fullpage({
      anchors: ['firstPage', 'publisher', 'advertiser', 'contact'],
      menu: '#menu',
      lockAnchors: true,
      navigation: true,
      navigationPosition: 'right',
      navigationTooltips: ['Home', 'Publisher', 'Advertiser', 'Contact Us'],
      showActiveTooltip: true,
      afterLoad: function (origin, destination, direction) {
        if (destination.index == 0) {
          $('#fp-nav ul li a span').attr('style', 'border-color: #e8e8e8 !important');
          $('#fp-nav ul li a.active span').attr('style', 'background: #e8e8e8 !important; border: 0 !important;');
          $('#fp-nav ul li .fp-tooltip').attr('style', 'color: #e8e8e8 !important')
          $('header').css('background', '#e8e8e8')
        }
        if (destination.index == 1) {
          $('#fp-nav ul li a span').attr('style', 'border-color: #1c83c4 !important');
          $('#fp-nav ul li a.active span').attr('style', 'background: #ee5567 !important; border: 0 !important;');
          $('#fp-nav ul li .fp-tooltip').attr('style', 'color: #ee5567 !important')
          $('header').css('background', '#ffffff')
        }
        if (destination.index == 2) {
          $('#fp-nav ul li a span').attr('style', 'border-color: #ee5567 !important');
          $('#fp-nav ul li a.active span').attr('style', 'background: #212a42 !important; border: 0 !important;');
          $('#fp-nav ul li .fp-tooltip').attr('style', 'color: #212a42 !important')
          $('header').css('background', '#e8e8e8')
        }
        if (destination.index == 3) {
          $('#fp-nav ul li a span').attr('style', 'border-color: #e8e8e8 !important');
          $('#fp-nav ul li a.active span').attr('style', 'background: #e8e8e8 !important; border: 0 !important;');
          $('#fp-nav ul li .fp-tooltip').attr('style', 'color: #e8e8e8 !important')
          $('header').css('background', '#e8e8e8')
        }
      }
    });
    $(document).keydown(function (event) {
      if (event.which === 9) { //disable Tab
        return false;
      }
    })


    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });


    $("form").submit(function () {
      var name = $("input[name=name]").val();
      var type = $("input[name=type]:checked").val();
      var email = $("input[name=email]").val();
      var message = $(".message").val();

      $.ajax({
        url: "{{ route('form') }}",
        data: {name: name, email: email, message: message,type:type},
        type: "PUT",
        success: function (data) {
          if(data.success){
            $('.alert-message').css({color:"#00e64d"}).text(data.message)
            $("input[name=name]").val('')
            $("input[name=email]").val('')
            $(".message").val('')
            setTimeout(function () {
              $('.alert-message').text('')
            },10000)
          }else{
            $('.alert-message').css({color:"#ec5466"}).text(data.message)
          }
        }
      });
    });


  });
</script>
</body>
</html>