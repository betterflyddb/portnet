<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Home\Home;
use Illuminate\Support\Facades\Mail;

class WebController extends Controller
{
    public function index()
    {
        $data = Home::find(1)->first();
        return view('index')->with(['data' => $data]);
    }

    public function form(Request $request)
    {
        $data = array('name' => $request->input('name'), "body" => '<br> Message: '.$request->input('message').' <br><br> '.' Type: '.$request->input("type"),'email' => $request->input('email'));

        try{
            Mail::send([], [], function ($message) use ($data) {
                $message->to('nino@port80.ge')
                    ->from($data['email'])
                    ->subject('PortNet Web')
                    ->setContentType('text/html')
                    ->setBody($data['body']);
            });

            return response()->json([ 'message' => 'Email Sent Successfuly' ,'success' => true]);
        }catch(\Exception $e){
            return response()->json([ 'message' => 'Unable to Send Email,try again later','success' => false]);
        }


    }
}
