<?php
namespace App\Modules\Home;


use BetterFly\Skeleton\Services\BaseService;

class HomeService extends BaseService{

  public function __construct(HomeRepository $repository){
    parent::__construct($repository);
  }
}