<?php
namespace App\Modules\Home;

    
use Illuminate\Database\Eloquent\Model;

class HomeTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["banner_title","banner_description","banner_image","publishers_title","box_title_1","box_title_2","box_title_3","box_title_4","box_title_5","box_title_6","clients_title","publishers_title","address","email","tel",];


}