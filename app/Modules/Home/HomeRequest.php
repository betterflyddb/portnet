<?php
namespace App\Modules\Home;

use BetterFly\Skeleton\App\Http\Requests\BaseFormRequest;

class HomeRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return $this->getRule();
    }

    /**
     * Custom message for validation
     *
     * @return  array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return  array
     */
    public function filters()
    {
        return [

        ];
    }

    private function getRule()
    {
        $type = $this->getMethod();

        switch($type)
        {
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                            'banner_title' => 'required|max:255',
                            'banner_description' => 'nullable|string',
                            'banner_image' => 'required|string',
                            'publishers_title' => 'nullable|max:255',
                            'box_title_1' => 'nullable|max:255',
                            'box_title_2' => 'nullable|max:255',
                            'box_title_3' => 'nullable|max:255',
                            'clients_title' => 'nullable|max:255',
                            'box_title_4' => 'nullable|max:255',
                            'box_title_5' => 'nullable|max:255',
                            'box_title_6' => 'nullable|max:255',
                            'address' => 'nullable|max:255',
                            'tel' => 'nullable|max:255',
                            'email' => 'nullable|max:255',
                        ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                            'banner_title' => 'required|max:255',
                            'banner_description' => 'nullable|string',
                            'banner_image' => 'required|string',
                            'publishers_title' => 'nullable|max:255',
                            'box_title_1' => 'nullable|max:255',
                            'box_title_2' => 'nullable|max:255',
                            'box_title_3' => 'nullable|max:255',
                            'clients_title' => 'nullable|max:255',
                            'box_title_4' => 'nullable|max:255',
                            'box_title_5' => 'nullable|max:255',
                            'box_title_6' => 'nullable|max:255',
                            'address' => 'nullable|max:255',
                            'tel' => 'nullable|max:255',
                            'email' => 'nullable|max:255',
                        ];
                }
            default:break;
        }
    }
}

