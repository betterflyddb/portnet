<?php
namespace App\Modules\Home;

use BetterFly\Skeleton\Repositories\BaseRepository;

class HomeRepository extends BaseRepository {
    public function __construct(Home $model){
        parent::__construct($model);
    }
}