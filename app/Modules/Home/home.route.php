<?php
Route::resource('home', 'App\Modules\Home\HomeController', [
    'names' => [
        'index' => 'home.index',
        'store' => 'home.store',
        'update' => 'home.update',
        'destroy' => 'home.delete'
    ]
]);
