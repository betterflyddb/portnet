<?php
namespace App\Modules\Home;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class HomeTransformer extends BaseTransformerAbstract
{
    public function transform($home)
    {
        $data = [
            'id' => $home->id,
            'banner_title' => $home->banner_title,
            'banner_description' => $home->banner_description,
            'banner_image' => $home->banner_image,
            'publishers_title' => $home->publishers_title,
            'box_title_1' => $home->box_title_1,
            'box_title_2' => $home->box_title_2,
            'box_title_3' => $home->box_title_3,
            'clients_title' => $home->clients_title,
            'box_title_4' => $home->box_title_4,
            'box_title_5' => $home->box_title_5,
            'box_title_6' => $home->box_title_6,
            'address' => $home->address,
            'tel' => $home->tel,
            'email' => $home->email,
        ];

        return $data;
    }
}