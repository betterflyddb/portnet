<?php

namespace App\Modules\Home;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'home';
    public $timestamps = false;
    protected $fillable = [""];

    use Translatable;

    public $translationModel = 'App\Modules\Home\HomeTranslation';
    public $translatedAttributes = ["banner_title", "banner_description", "banner_image", "publishers_title", "box_title_1", "box_title_2", "box_title_3", "box_title_4", "box_title_5", "box_title_6", "clients_title", "publishers_title", "address", "email", "tel",];


}